(function() {
	'use strict';

	angular
		.module('hackerNewsApp')
		.controller('storyController', storyController);


	storyController.$inject = ['$scope', '$http', '$routeParams'];
	function storyController($scope, $http, $routeParams) {

		$scope.id = $routeParams.id;
		$scope.story = null;
		$scope.comments = [];


		$http({
			method : 'GET',
			url : 'https://hacker-news.firebaseio.com/v0/item/' + $scope.id + '.json?print=pretty'
		}).then(function successCallback(response) {
			$scope.story = response.data;
			createComments($scope.comments, response.data.kids);
		});	


		var createComments = function(comments, data) {
			if(data == undefined || data.length === 0) {
				return;
			}	
			
			angular.forEach(data, function(value, key) {
				$http({
					method : 'GET',
					url : 'https://hacker-news.firebaseio.com/v0/item/' + value + '.json?print=pretty'
				}).then(function successCallback(response) {
					comments.push({"text" : response.data.text});
					comments[comments.length-1]["comments"] = [];
					comments[comments.length-1]["comments"].push(createComments(comments[comments.length -1]["comments"], response.data.kids));
				})
			});

		}


	}

})();