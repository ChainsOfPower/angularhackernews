(function() {
	'use strict';

	angular
		.module('hackerNewsApp')
		.controller('homeController', homeController);

	homeController.$inject = ['$scope', '$http', 'localStorageService'];
	function homeController($scope, $http, localStorageService) {

		$scope.N = 10;
		$scope.latestStoriesIds = [];
		$scope.stories = [];
		
		$http({
			method: 'GET',
			url: 'https://hacker-news.firebaseio.com/v0/newstories.json?print=pretty'
		}).then(function successCallback(response) {
			$scope.latestStoriesIds = response.data;
			for(var i = 0; i < $scope.N; i++) {
				$http({
					method : 'GET',
					url : 'https://hacker-news.firebaseio.com/v0/item/' + $scope.latestStoriesIds[i] + '.json?print=pretty'
				}).then(function successCallback(response) {
					response.data["favourite"] = localStorageService.get(response.data.id);
					$scope.stories.push(response.data);
				})
			}
		}, function errorCallback(response) {
			$scope.latestStoriesIds = [];
		});

		$scope.loadMoreStories = function() {
			var current = $scope.stories.length;
			for(var i = current; i < current + $scope.N; i++) {
				$http({
					method : 'GET',
					url : 'https://hacker-news.firebaseio.com/v0/item/' + $scope.latestStoriesIds[i] + '.json?print=pretty'
				}).then(function successCallback(response) {
					$scope.stories.push(response.data);
				})
			}
		}

		$scope.addToFavourites = function(story) {
			story.favourite = true;
			localStorageService.set(story.id, true);
		}

		$scope.removeFromFavourites = function(story) {
			story.favourite = false;
			localStorageService.remove(story.id);
		}
	}

})();