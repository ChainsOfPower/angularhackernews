(function(){
	'use strict';
	
	angular.module('hackerNewsApp')
	.config(routes);
	
	function routes($routeProvider){
		$routeProvider
			.when('/', {
				templateUrl : 'script/app/home/home.component.html',
				controller  : 'homeController'
			})
			.when('/story/:id', {
				templateUrl : 'script/app/story/story.component.html',
				controller 	: 'storyController'
			})
			.when('/favourites', {
				templateUrl : 'script/app/favourites/favourites.component.html',
				controller : 'favouritesController'
			});
	}

}) (); 