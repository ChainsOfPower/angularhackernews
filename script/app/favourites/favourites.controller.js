(function() {
	'use strict';

	angular
		.module('hackerNewsApp')
		.controller('favouritesController', favouritesController);

	favouritesController.$inject = ['$scope', '$http', 'localStorageService'];
	function favouritesController($scope, $http, localStorageService) {

		$scope.favouriteStoriesIds = localStorageService.keys();
		$scope.stories = [];
		
		console.log($scope.favouriteStoriesIds);

		$scope.removeFromFavourites = function(story) {
			localStorageService.remove(story.id);
			$scope.stories = $scope.stories.filter((item) => item.id !== story.id);
		}

		angular.forEach($scope.favouriteStoriesIds, function(value, key) {
				$http({
					method : 'GET',
					url : 'https://hacker-news.firebaseio.com/v0/item/' + value + '.json?print=pretty'
				}).then(function successCallback(response) {
					$scope.stories.push(response.data);
				})
		});

	}

})();